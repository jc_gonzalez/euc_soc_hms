#!/bin/bash 
#------------------------------------------------------------------------------------
# ares_retrimp.sh
# Launcher for ARES Retrieve/Import GUI tool
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYARES_INI_FILE=$HOME/.euclid/hktmprodgen/hktmprodgen-pyares.ini
export PYTHONPATH=${SCRIPTPATH}

if [ ! -f ${PYARES_INI_FILE} ]; then
    echo "Expected PyARES config. file at ${PYARES_INI_FILE} not found"
    exit 1
fi

if [ $# -lt 1 ]; then
    echo "usage: "
    echo "        $0 retrieve [ --help | <options> ]"
    echo "        $0 import [ --help | <options> ]"
    echo "        $0 gui "
    exit 0
fi

case $1 in
    gui)
        python3 ${SCRIPTPATH}/ares_retrimp/ares_retrimp_gui.py
        ;;
    retrieve)
        shift
        python3 ${SCRIPTPATH}/ares_retrimp/ares_retrimp.py $*
        ;;
    import)
        shift
        python3 ${SCRIPTPATH}/ares/importFiles.py $*
        ;;
     *)
        echo "Command not understood: $1"
        exit 1
        ;;
esac
